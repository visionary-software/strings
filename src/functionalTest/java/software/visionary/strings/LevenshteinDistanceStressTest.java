package software.visionary.strings;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import software.visionary.math.types.NaturalNumber;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

class LevenshteinDistanceStressTest {
    private static final List<String> WORDS = getWordList();

    @ParameterizedTest
    @MethodSource
    void stress(final String word) {
        assertTrue(min(word).isPresent());
    }

    public static Stream<Arguments> stress() {
        return WORDS.stream().map(Arguments::of);
    }

    private static Optional<NaturalNumber> min(final String s) {
        return WORDS.stream()
                .parallel()
                .filter(j -> !j.equals(s) && j.startsWith(s.substring(0,1)))
                .map(j -> LevenshteinDistance.INSTANCE.apply(s,j))
                .min(Comparator.naturalOrder());
    }

    private static List<String> getWordList() {
        try (final InputStream resource = LevenshteinDistanceStressTest.class.getResourceAsStream("/words");
             final InputStreamReader inputStreamReader = new InputStreamReader(resource, StandardCharsets.UTF_8);
             final BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            return bufferedReader.lines().collect(Collectors.toList());
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }
}
