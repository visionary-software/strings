/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.strings;

import software.visionary.exceptional.Exceptional;
import software.visionary.math.types.NaturalNumber;
import software.visionary.numbers.whole.NumberToWholeNumber;

import java.util.function.BiFunction;

/**
 * Calculates the <a href="https://en.wikipedia.org/wiki/Hamming_distance">
 *     Hamming Distance</a> (String Edit Distance) between 2 strings.
 * e.g. "lawn", "flaw" -> 4
 * Using a reduction after checking for equal length
 */
public enum HammingDistance implements BiFunction<String, String, NaturalNumber> {
    /**
     * SINGLETON.
     */
    INSTANCE;

    @Override
    public NaturalNumber apply(final String u, final String v) {
        Exceptional.demand(() -> u.length() == v.length());
        return (NaturalNumber) NumberToWholeNumber.INSTANCE.apply(StringToSequence.INSTANCE.apply(u, "").zip(StringToSequence.INSTANCE.apply(v, "")).stream()
                .reduce(
                        0,
                        (num, z) -> z.a().equals(z.b()) ? num : num+1,
                        Integer::sum
                ));
    }
}
