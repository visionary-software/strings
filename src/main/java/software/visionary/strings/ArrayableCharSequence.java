/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.strings;

import software.visionary.seqs.Sequence;
import software.visionary.traversal.Traversable;
import software.visionary.traversal.Traversal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Extends {@link CharSequence} with useful methods.
 */
public interface ArrayableCharSequence extends CharSequence, Sequence<Character> {
    /**
     * Why isn't this in the JDK?
     * @return the CharSequence as a CharArray
     */
    default char[] toCharArray() {
        final char[] result = new char[length()];
        for (int i = 0; i< length(); i++) {
            result[i] = charAt(i);
        }
        return result;
    }

    @Override
    default Traversal<Character> traverse() {
        final char[] primitive = toCharArray();
        final List<Character> boxed = new ArrayList<>(primitive.length);
        for (final char c : primitive) {
            boxed.add(c);
        }
        return Traversable.fromIterable(boxed).traverse();
    }
}
