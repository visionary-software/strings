/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.strings;

import software.visionary.math.types.NaturalNumber;

import java.util.Comparator;
import java.util.Objects;

/**
 * Useful when one is sure a string should not be null or empty.
 */
public final class NonEmptyString implements CharSequence, Comparable<NonEmptyString> {
    private final String letters;

    /**
     * Should not be null or empty.
     * @param input to be checked.
     */
    public NonEmptyString(final String input) {
        letters = Objects.requireNonNull(input);
        if(input.isEmpty() || input.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NonEmptyString name = (NonEmptyString) o;
        return Objects.equals(letters, name.letters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(letters);
    }

    @Override
    public int length() {
        return letters.length();
    }

    @Override
    public char charAt(final int index) {
        return letters.charAt(index);
    }

    @Override
    public CharSequence subSequence(final int start, final int end) {
        return letters.subSequence(start, end);
    }

    @Override
    public String toString() {
        return letters;
    }

    @Override
    public int compareTo(final NonEmptyString nonEmptyString) {
        return Comparator.<String>naturalOrder().compare(letters, nonEmptyString.letters);
    }

    /**
     * See {@link LevenshteinDistance}.
     * @param o should not be null
     * @return never null
     */
    public NaturalNumber levenschtein(final String o) {
        return LevenshteinDistance.INSTANCE.apply(this.toString(), o);
    }

    /**
     * See {@link HammingDistance}.
     * @param o should not be null
     * @return never null
     */
    public NaturalNumber hamming(final String o) {
        return HammingDistance.INSTANCE.apply(this.toString(), o);
    }
}
