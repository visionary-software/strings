/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.strings;

import software.visionary.math.types.NaturalNumber;
import software.visionary.numbers.whole.NumberToWholeNumber;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.function.BiFunction;

/**
 * Calculates the <a href="https://en.wikipedia.org/wiki/Levenshtein_distance">
 *     Levenshtein Distance</a> (String Edit Distance) between 2 strings.
 * e.g. "kitten", "sitting" -> 3
 * Using a {@link RecursiveTask} in the {@link ForkJoinPool#commonPool()}
 *
 */
public enum LevenshteinDistance implements BiFunction<String, String, NaturalNumber> {
    /**
     * SINGLETON.
     */
    INSTANCE;

    @Override
    public NaturalNumber apply(final String u, final String v) {
        return asNatural(TwoMatrixRows.distance(u,v));
    }

    private static NaturalNumber asNatural(final long number) {
        return (NaturalNumber) NumberToWholeNumber.INSTANCE.apply(number);
    }

    private enum TwoMatrixRows {
        ;
        public static int distance(final String x, final String y) {
            final int m = x.length();
            final int n = y.length();

            if (m == 0) {
                return n;
            }

            if (n == 0) {
                return m;
            }

            int[] prevRow = new int[n + 1];
            final int[] currRow = new int[n + 1];

            for (int j = 0; j <= n; j++) {
                prevRow[j] = j;
            }

            for (int i = 1; i <= m; i++) {
                currRow[0] = i;

                for (int j = 1; j <= n; j++) {
                    currRow[j] = (x.charAt(i - 1) == y.charAt(j - 1)) ? prevRow[j - 1] : 1 + Math.min(currRow[j - 1], Math.min(prevRow[j], prevRow[j - 1]));
                }
                prevRow = Arrays.copyOf(currRow, currRow.length);
            }
            return currRow[n];
        }
    }

    private static final class LevenshteinTask extends RecursiveTask<Integer> {
        private final String x,y;
        private final int m,n;

        private LevenshteinTask(final String x, final String y, int m, int n) {
            this.x = x;
            this.y = y;
            this.m = m;
            this.n = n;
        }

        @Override
        protected Integer compute() {
            if (m == 0) {
                return n;
            }
            if (n == 0) {
                return m;
            }
            if (x.charAt(m - 1) == y.charAt(n - 1)) {
                return new LevenshteinTask(x, y, m - 1, n -1).compute();
            }
            final ForkJoinTask<Integer> insert = new LevenshteinTask(x,y, m, n- 1).fork();
            final ForkJoinTask<Integer> remove = new LevenshteinTask(x,y, m - 1, n).fork();
            final ForkJoinTask<Integer> replace = new LevenshteinTask(x,y, m - 1, n - 1).fork();
            return 1 + Math.min(insert.join(), Math.min(remove.join(), replace.join()));
        }
    }

    private static final class OldShittyLevenschteinTask extends RecursiveTask<NaturalNumber> {
        private final String u,v;

        private OldShittyLevenschteinTask(final String u, final String v) {
            this.u = u;
            this.v = v;
        }

        @Override
        protected NaturalNumber compute() {
            if(v.isEmpty()) {
                return asNatural(u.length());
            } else if(u.isEmpty()) {
                return asNatural(v.length());
            } else {
                return (head(u).equals(head(v))) ? new OldShittyLevenschteinTask(tail(u), tail(v)).compute() : minDistanced(u,v);
            }
        }

        private String head(final String s) {
            return s.substring(0,1);
        }

        private String tail(final String s) {
            return s.substring(1);
        }

        private NaturalNumber minDistanced(final String u, final String v) {
            return (NaturalNumber) software.visionary.numbers.whole.NaturalNumber.ONE.add(
                    delete().join())
                    .min(insert().join())
                    .min(replace().join()
                    );
        }

        private ForkJoinTask<NaturalNumber> delete() {
            return new OldShittyLevenschteinTask(tail(u), v).fork();
        }

        private ForkJoinTask<NaturalNumber> insert() {
            return new OldShittyLevenschteinTask(u, tail(v)).fork();
        }

        private ForkJoinTask<NaturalNumber> replace() {
            return new OldShittyLevenschteinTask(tail(u), tail(v)).fork();
        }
    }
}
