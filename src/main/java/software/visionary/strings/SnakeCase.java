/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
package software.visionary.strings;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * "this becomes" -> this_becomes.
 */
public enum SnakeCase implements Function<String, String> {
	/**
	 * SINGLETON.
	 */
	INSTANCE;

	@Override
	public String apply(final String s) {
		if (!s.contains(" ")) {
			return s;
		}
		final String[] split = s.split(" ");
		return Arrays.stream(split).map(String::toLowerCase).collect(Collectors.joining("_"));
	}
}
