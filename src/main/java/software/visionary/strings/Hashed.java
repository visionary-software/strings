/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.strings;

import java.util.Objects;

/**
 * A special kind of String that should never reveal itself in plain-text.
 * Rather than storing the whole String, this stores a hash from a static hash function.
 * These can be checked for equality or sorted without knowing their contents.
 * Use case: passwords.
 */
public final class Hashed implements ArrayableCharSequence, Comparable<Hashed> {
    private final int value;

    /**
     * From a non-null or empty string.
     * @param clearText should not be null or empty
     * @return never null
     */
    public static Hashed nonNullOrEmpty(final String clearText) {
        return new Hashed(new NonEmptyString(clearText));
    }

    private Hashed(final NonEmptyString clearText) {
        value = Objects.hash(clearText);
    }

    @Override
    public int length() {
        return String.valueOf(value).length();
    }

    @Override
    public char charAt(final int i) {
        return String.valueOf(value).charAt(i);
    }

    @Override
    public CharSequence subSequence(final int begin, final int end) {
        return String.valueOf(value).subSequence(begin, end);
    }

    @Override
    public int compareTo(final Hashed hashed) {
        return Integer.compare(value, hashed.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hashed hashed = (Hashed) o;
        return value == hashed.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
