/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.strings;

import java.util.Comparator;
import java.util.Objects;

/**
 * A Name is a distinct type of String that should not be null or empty and is used as an identifier or label.
 */
public final class Name implements CharSequence, Comparable<Name> {
    private final NonEmptyString letters;

    /**
     * checked to make sure it is not null or empty.
     * @param input to be checked
     */
    public Name(final String input) {
        this(new NonEmptyString(input));
    }

    /**
     * checked to make sure it is not null
     * @param string to be checked
     */
    public Name(final NonEmptyString string) {
        this.letters = Objects.requireNonNull(string);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Name name = (Name) o;
        return Objects.equals(letters, name.letters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(letters);
    }

    @Override
    public int length() {
        return letters.length();
    }

    @Override
    public char charAt(final int index) {
        return letters.charAt(index);
    }

    @Override
    public CharSequence subSequence(final int start, final int end) {
        return letters.subSequence(start, end);
    }

    @Override
    public String toString() {
        return letters.toString();
    }

    @Override
    public int compareTo(final Name name) {
        return Comparator.<String>naturalOrder().compare(letters.toString(), name.letters.toString());
    }
}
