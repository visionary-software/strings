/*
 * Copyright (c) 2024.
 *
 * Nico Vaidyanathan Hidalgo Visionary Software Solutions LLC
 *
 * This file is part of Visionary Software Solutions STRINGS.
 *  Visionary Software Solutions STRINGS is free software:
 *  you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License
 *  as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later version.
 *
 *  Visionary Software Solutions STRINGS
 *  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Visionary Software Solutions STRINGS.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

package software.visionary.strings;

import java.util.LinkedList;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Breaks apart a {@link String} into {@link Iterable<String>}.
 * Stream the {@link Character} and put them into a {@link LinkedList}.
 * Surprising the JDK doesn't offer this by default, and that CharSequence isn't iterable...
 */
public enum StringToIterable implements Function<String, Iterable<String>> {
    /**
     * SINGLETON.
     */
    INSTANCE;

    @Override
    public Iterable<String> apply(final String s) {
        return s.chars().mapToObj(i -> String.valueOf(Character.valueOf((char) i))).collect(Collectors.toCollection(LinkedList::new));
    }
}
