/**
 * Useful utilities for Strings.
 */
module software.visionary.strings {
	requires transitive software.visionary.seqs;
    requires software.visionary.numbers;
    exports software.visionary.strings;
}