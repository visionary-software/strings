package software.visionary.strings;

import org.junit.jupiter.api.*;
import software.visionary.seqs.Sequence;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class StringToSequenceTest {
    @Test
    void canConvertAStringIntoASequence() {
        final String dog = "GOOD DOG";
        final Sequence<String> result = StringToSequence.INSTANCE.apply(dog, " ");
        final List<String> expected = List.of("GOOD", "DOG");
        final AtomicInteger i = new AtomicInteger();
        result.forEach(s -> assertEquals(expected.get(i.getAndIncrement()), s));
    }
}
