package software.visionary.strings;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringToIterableTest {
    @Test
    void canConvertAStringIntoAnIterable() {
        final String dog = "DOG";
        final Iterable<String> result = StringToIterable.INSTANCE.apply(dog);
        final String[] expected = new String[] { "D", "O", "G"};
        final AtomicInteger i = new AtomicInteger();
        result.forEach(s -> assertEquals(expected[i.getAndIncrement()], s));
    }
}
