package software.visionary.strings;

import org.junit.jupiter.api.*;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class ReverseTest {
    @Test
    void canReverse() {
        final String dog = "DOG";
        final String result = Reverse.INSTANCE.apply(dog);
        final String expected = "GOD";
        assertEquals(expected, result);
    }
}
