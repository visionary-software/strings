package software.visionary.strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import software.visionary.seqs.Sequence;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class ArrayableCharSequenceTest {
    private static final class Dog implements ArrayableCharSequence {
        private static final String dog = "Shammee";
        @Override
        public int length() {
            return dog.length();
        }

        @Override
        public char charAt(int i) {
            return dog.charAt(i);
        }

        @Override
        public CharSequence subSequence(int i, int i1) {
            return dog.subSequence(i, i1);
        }
    }

    @Test
    void canConvertToCharArry() {
        final char[] result = new Dog().toCharArray();
        final char[] expected = new char[] { 'S', 'h', 'a', 'm', 'm', 'e', 'e'};
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    void canMakeSequenceOfCharacter() {
        final Sequence<Character> result = () -> new Dog().traverse();
        final List<Character> expected = List.of('S', 'h', 'a', 'm', 'm', 'e', 'e');
        final AtomicInteger counter = new AtomicInteger();
        result.forEach(c -> assertEquals(expected.get(counter.getAndIncrement()), c));
    }
}
