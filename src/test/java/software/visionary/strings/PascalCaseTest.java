package software.visionary.strings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PascalCaseTest {
	@Test
	void garbageInGarbageOut() {
		assertThrows(NullPointerException.class, () -> PascalCase.INSTANCE.apply(null));
	}

	@Test
	void noEffectIfNoSpaces() {
		final String input = "judymarcela";
		assertEquals(input, PascalCase.INSTANCE.apply(input));
	}

	@Test
	void pascalCasesForSpaces() {
		final String input = "pachis campos";
		assertEquals("PachisCampos", PascalCase.INSTANCE.apply(input));
	}
}
