package software.visionary.strings;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class SnakeCaseTest {
	@Test
	void garbageInGarbageOut() {
		assertThrows(NullPointerException.class, () -> PascalCase.INSTANCE.apply(null));
	}

	@Test
	void noEffectIfNoSpaces() {
		final String input = "JudyMarcela";
		assertEquals(input, SnakeCase.INSTANCE.apply(input));
	}

	@Test
	void pascalCasesForSpaces() {
		final String input = "pachis campos";
		assertEquals("pachis_campos", SnakeCase.INSTANCE.apply(input));
	}
}
