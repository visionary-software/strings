package software.visionary.strings;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import software.visionary.exceptional.DemandFailedException;
import software.visionary.math.types.NaturalNumber;
import software.visionary.numbers.whole.NumberToWholeNumber;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class HammingDistanceTest {

    @Test
    void rejectsUnequalLength() {
        assertThrows(DemandFailedException.class, () -> HammingDistance.INSTANCE.apply("", "b"));
        assertThrows(DemandFailedException.class, () -> HammingDistance.INSTANCE.apply("c", "da"));
    }

    @ParameterizedTest
    @MethodSource
    void canCalculate(final String a, final String b, final NaturalNumber expected) {
        assertEquals(expected, HammingDistance.INSTANCE.apply(a, b));
    }

    public static Stream<Arguments> canCalculate() {
        return Stream.of(
                Arguments.of("lawn", "flaw", NumberToWholeNumber.INSTANCE.apply(4)),
                Arguments.of("karolin", "kathrin", NumberToWholeNumber.INSTANCE.apply(3)),
                Arguments.of("karolin",  "kerstin", NumberToWholeNumber.INSTANCE.apply(3)),
                Arguments.of("kathrin", "kerstin" , NumberToWholeNumber.INSTANCE.apply(4)),
                Arguments.of("0000", "1111", NumberToWholeNumber.INSTANCE.apply(4)),
                Arguments.of("2173896", "2233796", NumberToWholeNumber.INSTANCE.apply(3))
        );
    }
}
