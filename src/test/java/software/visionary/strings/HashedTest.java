package software.visionary.strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

class HashedTest {
    @Test
    void equivalentLength() {
        final String input = "fuck";
        final Hashed p = Hashed.nonNullOrEmpty(input);
        final String hashed = String.valueOf(Objects.hash(input));
        Assertions.assertEquals(hashed.length(), p.length());
    }

    @Test
    void equivalentCharAt() {
        final String input = "shit";
        final Hashed p = Hashed.nonNullOrEmpty(input);
        final String hashed = String.valueOf(Objects.hash(input));
        Assertions.assertEquals(hashed.charAt(1), p.charAt(1));
    }

    @Test
    void equivalentSubSequence() {
        final String input = "joder";
        final Hashed p = Hashed.nonNullOrEmpty(input);
        final String hashed = String.valueOf(Objects.hash(input));
        Assertions.assertEquals(hashed.subSequence(0, 2), p.subSequence(0,2));
    }

    @Test
    void correctlyAndStablyImplementsHashCode() {
        final String input = "echar un polvo";
        final Hashed p = Hashed.nonNullOrEmpty(input);
        Assertions.assertEquals(p.hashCode(), p.hashCode());
        final String other = "echo un cohete";
        final Hashed p2 = Hashed.nonNullOrEmpty(other);
        Assertions.assertNotEquals(p.hashCode(), p2.hashCode());
        final Hashed same = Hashed.nonNullOrEmpty(input);
        Assertions.assertEquals(p.hashCode(), same.hashCode());
    }

    @Test
    void correctlyAndStablyImplementsEquals() {
        final String input = "garchar";
        final Hashed p = Hashed.nonNullOrEmpty(input);
        Assertions.assertTrue(p.equals(p));
        Assertions.assertFalse(p.equals(null));
        Assertions.assertFalse(p.equals(new Object()));
        final Hashed same = Hashed.nonNullOrEmpty(input);
        Assertions.assertTrue(p.equals(same));
        final String other = "triskar";
        final Hashed p2 = Hashed.nonNullOrEmpty(other);
        Assertions.assertFalse(p.equals(p2));
    }

    @Test
    void correctlyImplementsCompareTo() {
        final String input = "hostia";
        final Hashed p = Hashed.nonNullOrEmpty(input);
        Assertions.assertEquals(0, p.compareTo(p));
        Assertions.assertThrows(NullPointerException.class, () -> p.compareTo(null));
        final Hashed same = Hashed.nonNullOrEmpty(input);
        Assertions.assertEquals(0, p.compareTo(same));
        final String other = "puta";
        final Hashed p2 = Hashed.nonNullOrEmpty(other);
        Assertions.assertEquals(Integer.compare(Objects.hash(input), Objects.hash(other)), p.compareTo(p2));
    }

    /**
     * Future modifications should never compromise the security of the password.
     */
    @Test
    void toStringDoesNotCompromiseSecurity() {
        final String input = "cojones";
        final Hashed p = Hashed.nonNullOrEmpty(input);
        Assertions.assertFalse(p.toString().contains(input));
        Assertions.assertFalse(p.toString().contains(String.valueOf(Objects.hash(input))));
    }
}
