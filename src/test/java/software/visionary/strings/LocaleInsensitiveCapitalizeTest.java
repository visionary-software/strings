package software.visionary.strings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LocaleInsensitiveCapitalizeTest {
	@Test
	void garbageInGarbageOut() {
		assertThrows(NullPointerException.class, () -> LocaleInsensitiveCapitalize.INSTANCE.apply(null));
	}

	@Test
	void noEffectOnAlreadyCapitalized() {
		final String input = "Judy";
		assertEquals(input, LocaleInsensitiveCapitalize.INSTANCE.apply(input));
	}

	@Test
	void capitalizes() {
		final String input = "pachis";
		assertEquals("Pachis", LocaleInsensitiveCapitalize.INSTANCE.apply(input));
	}
}
