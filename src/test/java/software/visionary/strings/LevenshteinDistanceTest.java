package software.visionary.strings;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import software.visionary.math.types.NaturalNumber;
import software.visionary.numbers.whole.NumberToWholeNumber;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LevenshteinDistanceTest {
    @ParameterizedTest
    @MethodSource
    void canCalculate(final String a, final String b, final NaturalNumber expected) {
        assertEquals(expected, LevenshteinDistance.INSTANCE.apply(a, b));
    }

    public static Stream<Arguments> canCalculate() {
        return Stream.of(
                Arguments.of("kitten", "sitting", NumberToWholeNumber.INSTANCE.apply(3)),
                Arguments.of("uninformed", "uniformed", software.visionary.numbers.whole.NaturalNumber.ONE),
                Arguments.of("", "bear", NumberToWholeNumber.INSTANCE.apply(4)),
                Arguments.of("turtle", "", NumberToWholeNumber.INSTANCE.apply(6)),
                Arguments.of("whale", "whale", software.visionary.numbers.whole.NaturalNumber.ZERO)
        );
    }
}
