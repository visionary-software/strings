package software.visionary.strings;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

final class NonEmptyStringTest {
    @Test
    void doesNotAllowNull() {
        assertThrows(NullPointerException.class, () -> new NonEmptyString(null));
    }

    @Test
    void doesNotAllowEmpty() {
        assertThrows(IllegalArgumentException.class, () -> new NonEmptyString(""));
    }

    @Test
    void doesNotAllowOnlyWhitespace() {
        assertThrows(IllegalArgumentException.class, () -> new NonEmptyString("   "));
        assertThrows(IllegalArgumentException.class, () -> new NonEmptyString(String.format("\t\t %n")));
    }

    @Test
    void lengthDelegates() {
        final String string = "cool stuff";
        final NonEmptyString toTest = new NonEmptyString(string);
        assertEquals(string.length(), toTest.length());
    }

    @Test
    void charAtDelegates() {
        final String string = "cool stuff";
        final NonEmptyString toTest = new NonEmptyString(string);
        for (int i = 0; i< string.length(); i++) {
            assertEquals(string.charAt(i), toTest.charAt(i));
        }
    }

    @Test
    void subSequenceDelegates() {
        final String string = "cool stuff";
        final NonEmptyString toTest = new NonEmptyString(string);
        final CharSequence subFromString = string.substring(0, 2);
        final CharSequence subFromNonEmptyString = toTest.subSequence(0, 2);
        assertEquals(subFromString, subFromNonEmptyString);
    }

    @Test
    void hashCodeIsFromSource() {
        final String string = "cool stuff";
        final NonEmptyString toTest = new NonEmptyString(string);
        assertEquals(Objects.hash(string), toTest.hashCode());
    }

    @Test
    void equalsContractIsCorrect() {
        final String string = "cool stuff";
        final NonEmptyString toTest = new NonEmptyString(string);
        assertFalse(toTest.equals(null));
        assertFalse(toTest.equals(string));
        assertTrue(toTest.equals(toTest));
        final NonEmptyString shouldEqual = new NonEmptyString(string);
        assertTrue(toTest.equals(shouldEqual));
        assertTrue(shouldEqual.equals(toTest));
    }


    @Test
    void toStringDelegates() {
        final String string = "cool stuff";
        final NonEmptyString toTest = new NonEmptyString(string);
        assertEquals(string, toTest.toString());
    }

    @Test
    void canCompareTo() {
        final NonEmptyString first = new NonEmptyString("a");
        assertEquals(0, first.compareTo(first));
        final NonEmptyString second = new NonEmptyString("b");
        assertEquals(-1, first.compareTo(second));
        assertEquals(1, second.compareTo(first));
        assertThrows(NullPointerException.class, () -> first.compareTo(null));
    }

    @ParameterizedTest
    @MethodSource
    void calculatesLevenschtein(final NonEmptyString a, final NonEmptyString b) {
        assertEquals(LevenshteinDistance.INSTANCE.apply(a.toString(), b.toString()), a.levenschtein(b.toString()));
    }

    public static Stream<Arguments> calculatesLevenschtein() {
        return Stream.of(
                Arguments.of(new NonEmptyString("kitten"), new NonEmptyString("sitting")),
                Arguments.of(new NonEmptyString("uninformed"), new NonEmptyString("uniformed")),
                Arguments.of(new NonEmptyString("whale"), new NonEmptyString("whale"))
        );
    }

    @ParameterizedTest
    @MethodSource
    void calculatesHamming(final NonEmptyString a, final NonEmptyString b) {
        assertEquals(HammingDistance.INSTANCE.apply(a.toString(), b.toString()), a.hamming(b.toString()));
    }

    public static Stream<Arguments> calculatesHamming() {
        return Stream.of(
                Arguments.of(new NonEmptyString("lawn"), new NonEmptyString("flaw")),
                Arguments.of(new NonEmptyString("karolin"), new NonEmptyString("kathrin")),
                Arguments.of(new NonEmptyString("0000"), new NonEmptyString("1111"))
        );
    }
}
