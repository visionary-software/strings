# STRINGS!

## WTF, Just use Apache Commons Lang or Guava like a reasonable person
Those things are gigantic, mah breh. I like small and focused libraries.
I like to use modern language features. I also have a better design sense,
honed through graduate study and years of personal development education in the literature,
than many of the authors of these libraries. 
-- I actually know what the Distance from the Main Sequence is and try to actively
apply SOLID and import design principles like TELL, DON'T ASK in my everyday practice.
I'm not gonna be "faux humble" about it, it's been a lot of work.
## Yeah, whatever. So what does it do?
1. NonEmptyString and Name -- use fucking types for your goddamn Contracts!
2. Hashed - Convenience object usable for sensitive information such as passwords.
3. ArrayableCharSequence -- You'd think this would be in the JDK.
4. PascalCase and LocalInsensitiveCapitalize -- Useful for Java code generation for when you want to follow convention
5. Reversed and SnakeCase -- You'd think...
6. Levenschtein & Hamming Distance -- useful for comparing strings
